def name(n, end):
    return str(n).rjust(2, '0') + '.' + end


class TestCreator:
    def __init__(self, dir='tests', test_file_end='tst', ans_file_end='ans'):
        self.dir = dir
        self.id = 1
        self.test_file_end = test_file_end
        self.ans_file_end = ans_file_end
        self.used_id = {self.test_file_end: [], self.ans_file_end: []}

    def update_id(self, end):
        while self.id in self.used_id[end]:
            self.id += 1

    def open(self, file_name, type):
        return open(self.dir + '\\' + file_name, type)

    def create_file(self, id, text, end):
        file = self.open(name(id, end), 'w')
        file.write(text)
        file.close()
        self.used_id[end].append(id)

    def create_test(self, id, text):
        self.create_file(id, text, self.test_file_end)

    def create_answer(self, id, text):
        self.create_file(id, text, self.ans_file_end)

    def create_next_file(self, text, end):
        self.update_id(end)
        self.create_file(self.id, text, end)

    def create_next_test(self, text):
        self.create_next_file(text, self.test_file_end)

    def create_next_ans(self, text):
        self.create_next_file(text, self.ans_file_end)

    def create_ans_with_test_file(self, id, function):
        test_file = self.open(name(id, self.test_file_end), 'r')
        test_text = test_file.read()
        self.create_file(id, str(function(test_text)), self.ans_file_end)
        test_file.close()

    def create_next_ans_by_test_file(self, function):
        self.update_id(self.ans_file_end)
        self.create_ans_with_test_file(id, function)

    def create_all_ans_files(self, function, ignore=[]):
        self.update_id(self.test_file_end)
        for i in range(1, self.id):
            if i not in ignore:
                self.create_ans_with_test_file(i, function)


if __name__ == "__main__":
    test_creator = TestCreator()
    for i in range(50):
        test_creator.create_next_test(str(i) + ' test')
        test_creator.create_next_ans(str(i) + ' ans')


