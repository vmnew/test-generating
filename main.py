from gentests.main import TestCreator
from random import randrange as rd


def solve(text):
    a, b = [int(i) for i in text.split()]
    return a + b


if __name__ == '__main__':
    test_creator = TestCreator(dir='gentests\\tests')
    for i in range(20):
        test_creator.create_next_test(str(rd(20)) + ' ' + str(rd(20)))
    test_creator.create_all_ans_files(solve)